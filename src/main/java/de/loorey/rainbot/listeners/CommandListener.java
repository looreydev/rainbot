package de.loorey.rainbot.listeners;

import de.loorey.rainbot.commands.HelpCMD;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.awt.*;

public class CommandListener extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String prf = "rb!";
        String[] args = event.getMessage().getContentRaw().split(" ");

        if(!event.getAuthor().isBot()) {
            if(args[0].equalsIgnoreCase(event.getJDA().getSelfUser().getAsMention())) {
                EmbedBuilder embed = new EmbedBuilder()
                        .setTitle("Hello there!")
                        .setColor(Color.WHITE)
                        .setFooter("Requested by "+event.getAuthor().getName(), event.getAuthor().getAvatarUrl())
                        .setDescription("My name is RainBot, im a multifunctional discord bot created and maintained by "+event.getJDA().getUserById(254225860649943051L).getName()+"\n\n" +
                                "My current Prefix is: `"+prf+"`\n"+
                                "My current ping is: `"+Math.round(event.getJDA().getPing())+"`\n\n"+
                                "You can get help and see all of my commands with using `"+prf+"help`!");
                event.getChannel().sendMessage(embed.build()).queue();
            } else {
                switch (args[0]) {
                    case "rb!help":
                        HelpCMD.run(event);
                        break;
                }
            }
        }
    }

}
