package de.loorey.rainbot.commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;

public class HelpCMD {

    public static void run(GuildMessageReceivedEvent event) {
        TextChannel channel = event.getChannel();
        User author = event.getAuthor();

        EmbedBuilder embed = new EmbedBuilder()
                .setTitle("All of my current commands")
                .setColor(Color.WHITE)
                .addField("rb!help", "Prints out this message", false)
                .setFooter("Requested by "+author.getName(), author.getAvatarUrl());
        channel.sendMessage(embed.build()).queue();
    }

}
